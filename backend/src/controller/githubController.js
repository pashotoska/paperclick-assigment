const octokit = require('@octokit/rest')();
const User= require('./../models/User');
const JWT   = require('jsonwebtoken');

module.exports={
    /**
     * Login Method
     * This method get username and password from user and check it with gihub. 
     * If user is registered on github it makes an check of our database with username
     * if user exists it just retrive some data and generate jwt token
     * if user doesn't exists it saved on database
     */
    login:(req, reply)=>{
        const server = req.server;
        var params = req.payload;
        //set authenticatation
        octokit.authenticate({
            type: 'basic',
            username: params.username,
            password: params.password
        });
        return new Promise((resolve) => {
            //check if user is authenticated
            octokit.users.get({},(error, result)=>{
                if(error){
                    // user doesn't exist on github
                    const response = reply.response({
                        "code":400,
                        "message":"Bad Credentials"
                    });
                    response.type('application/json');
                    resolve(response);
                }else{
                    server.bind({octokit:octokit}); //save this for other use on this controller
                    //check if user exist on database
                    User.findOne({ username: params.username }, function (err, u) {
                        if(err){
                            // if we get an error from database return this error
                            const response = reply.response({
                                "code":400,
                                "message":err.message,
                                "error":err
                            });
                            response.type('application/json');
                            resolve(response);
                        }else if(u == null){
                            //create new user
                            const userData = result.data;
                            var newUser = new User({ 
                                name: userData.name,
                                username:params.username,
                                repos_url:userData.repos_url, 
                                company:userData.company,
                                external_id:userData.id
                            });
                            //save this user on database
                            newUser.save(function(err){
                                if(err){
                                    const response = reply.response({
                                        "code":400,
                                        "message":err.message,
                                        "error":err
                                    });
                                    response.type('application/json');
                                    resolve(response);
                                }else{
                                    
                                    var userObj = {
                                        id : newUser._id,
                                        name : newUser.name,
                                        external_id : newUser.external_id
                                    };
                                    const token = JWT.sign(userObj, "ThisNeedsToBeSecretButForThisProjectSeemsToBeFine");
                                    userObj.token = token;
                                    
                                    const response = reply.response({
                                        "code":200,
                                        "message":"User Successfuly logged in",
                                        "user":userObj
                                    });
                                    response.type('application/json');
                                    resolve(response); //return user object with token and some successfuly messages
                                }
                            });
                        }else{
                            // users exist on database and now generate jwt token
                            var userObj = {
                                id : u._id,
                                name : u.name,
                                external_id : u.external_id
                            };
                            const token = JWT.sign(userObj, "ThisNeedsToBeSecretButForThisProjectSeemsToBeFine");
                            userObj.token = token;
                            
                            const response = reply.response({
                                "code":200,
                                "message":"User Successfuly logged in",
                                "user":userObj
                            });
                            response.type('application/json');
                            resolve(response);
                        }
                    });
                }
            });
        });
    },
    /**
     * Profile
     * This method will get and return gihub user profile
     */
    profile:(req, reply)=>{
        const authOctokit = req.server.realm.settings.bind.octokit;
        return new Promise((resolve) => {
            if(typeof authOctokit !="undefined"){
                //check if user is authenticated
                authOctokit.users.get({},(error, result)=>{
                    if(error){
                        // user doesn't exist on github
                        const response = reply.response({
                            "code":400,
                            "message":"Bad Credentials"
                        });
                        response.type('application/json');
                        resolve(response);
                    }else{
                        //return user profile
                        const response = reply.response({
                            "code":200,
                            "message":"Profile is ok",
                            "user":result.data
                        });
                        response.type('application/json');
                        resolve(response);
                    }
                });
            }else{
                const response = reply.response({
                    "code":401,
                    "message":"Unauthorized"
                });
                response.type('application/json');
                resolve(response);
            }
        });
    },
    getStarredRepo:(req,reply)=>{
        const authOctokit = req.server.realm.settings.bind.octokit;
        var params = req.query;
        if(typeof params.per_page == "undefined" || (1*params.per_page > 100 && 1*params.per_page < 1 )) params.per_page = 30;
        if(typeof params.page == "undefined" || 1* params.page < 1) params.page = 1;
        if(typeof params.direction == "undefined" || (params.direction != "desc" && params.direction != "asc")) params.direction = 'desc';
        if(typeof params.sort == "undefined" || (params.direction != "created" && params.direction != "updated")) params.sort = 'created';
        
        return new Promise((resolve) => {
            if(typeof authOctokit !="undefined"){
                //check if user is authenticated
                authOctokit.activity.getStarredRepos({
                    sort:params.sort, 
                    direction:params.direction, 
                    per_page:params.per_page, 
                    page:params.page
                }, (error, result) =>{
                    if(error){
                        // user doesn't exist on github
                        const response = reply.response({
                            "code":400,
                            "message":"Bad Credentials"
                        });
                        response.type('application/json');
                        resolve(response);
                    }else{
                        console.log(result);
                        //return user profile
                        const response = reply.response({
                            "code":200,
                            "message":"Success",
                            "repos":result.data
                        });
                        response.type('application/json');
                        resolve(response);
                    }
                });
            }else{
                const response = reply.response({
                    "code":401,
                    "message":"Unauthorized"
                });
                response.type('application/json');
                resolve(response);
            }
        });

    }
}