const Hapi = require( "hapi" );
const routes = require( "./routes" );
const plugins = require( "./plugins" );
const DEV_PORT = 8000;


const server = Hapi.server( {
	port: process.env.PORT || DEV_PORT
} );
async function start() {
	try {
		// register plugins
		await plugins.register( server );

		// register routes
		await routes.register( server );

		// start the server
        await server.start();
        console.log("Server is runnin on port: "+(process.env.PORT || DEV_PORT));
	} catch ( err ) {
        console.log(err);
		process.exit( 1 ); // eslint-disable-line
	}
}

start();