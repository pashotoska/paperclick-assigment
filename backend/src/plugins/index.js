"use strict";

const inert = require( "inert" );
const init_db = require("./initDb");
const auth = require("./auth");

const register = async server => {
    await server.register(inert);
    await auth.register(server);
    await init_db.register(server);
};

module.exports = {
	register
};