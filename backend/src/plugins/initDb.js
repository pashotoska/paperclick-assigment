var Mongoose = require('mongoose');
try {
    require.resolve("../config/db");
} catch(e) {
    console.log("YOU need to make changes on config folder. Remove .example from db.js file and add your database configuration file");
    process.exit(e.code);
}
const CONFIG_DB = require("../config/db");
const register = async server => {
    if (!CONFIG_DB.host) {
        throw new Error('DB host not defined in config');
    }

    Mongoose.connect(CONFIG_DB.host+CONFIG_DB.database,{ useNewUrlParser: true });
    
    var db = Mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function callback() {
        console.log('Connection with database succeeded.');
    });
    server.bind({db: db});
};

module.exports = {
    register
};