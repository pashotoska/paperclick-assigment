var UserModel = require('./../models/User');

const validate = async function (decoded, request) {
    const authOctokit = request.server.realm.settings.bind.octokit;
    if(typeof authOctokit == "undefined"){
        return {isValid:false};
    }
    return { isValid: true };
};

const register = async server => {
    await server.register(require('hapi-auth-jwt2'));
    server.auth.strategy('jwt', 'jwt',
    {   key: 'ThisNeedsToBeSecretButForThisProjectSeemsToBeFine',          // Never Share your secret key
        validate: validate,            // validate function defined above
        verifyOptions: { algorithms: [ 'HS256' ] } // pick a strong algorithm
    });
    
    server.auth.default('jwt');
};

module.exports = {
    register
};