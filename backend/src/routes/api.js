"use strict";
var githubController = require("./../controller/githubController");
const Joi = require('joi');

module.exports = server => {
	server.route( {
		method: "POST",
        path: "/api/github/login",
		config: { 
			auth: false, 
			validate: {
				payload: {
					username: Joi.string().required(),
					password: Joi.string().required()
				}
			},
			cors: {
				origin: ['*'],
				additionalHeaders: ['cache-control', 'x-requested-with']
			}
		},
		handler:githubController.login
	} );
	server.route( {
		method: "GET",
		path: "/api/github/user/profile",
        config: { 
			auth: 'jwt',
			cors: {
				origin: ['*'],
				additionalHeaders: ['cache-control', 'x-requested-with']
			}
		},
		handler:githubController.profile
    } );
    
	server.route( {
		method: "GET",
		path: "/api/github/user/repo/starred",
        config: { 
			auth: 'jwt',
			validate:{
				query:{
					per_page:Joi.number(),
					page:Joi.number(),
					direction:Joi.string(),
					sort:Joi.string()
				}
			},
			cors: {
				origin: ['*'],
				additionalHeaders: ['cache-control', 'x-requested-with']
			}
		},
		handler:githubController.getStarredRepo
	} );
}
;