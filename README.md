**Node Js Assigment for PaperClick**

---

## Structure

You’ll find two folders into ths project **backend** and **assigment-front**.

1. On backend folder it is made the core of backend on nodejs And Hapijs framework and MongoDb.
2. Assigment-front it is and vue js project(I haven't made it build for you to see the code).

---

## Getting Started with Backend

After you clone this repository you need to run commands like **cd backend** and after that **npm install** to install node modules.
You need to config and database on config/db.js after that you can run **npm start** to start yuor project.
If you go on http://localhost:8000 you will see like an documentation of rest api methods. 
Inside of this directory is an Docker file to create an virtual docker environment for this project. 
To be able to run and build this docker image make sure to have docker installed and run command **docker-compose up**

---

## Getting Started with Frontend

After you clone this repository you need to run commands like **cd assigment-front** and after that **npm install** to install node modules.
Run **npm run serve** to start yuor project.
If you go on http://localhost:8080 you will see login screen where you need to put your gihub credentials to be able to login.
After that you will see listed your profile and an Button to retrive starred repo.

