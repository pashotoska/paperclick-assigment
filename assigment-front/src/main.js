import Vue from 'vue';
import App from './App.vue';
import VueResource from 'vue-resource';
import VeeValidate from 'vee-validate';
import VueMoment from 'vue-moment';
import moment from 'moment';

import { store } from './_store';
import { router } from './_helpers';

Vue.use(VeeValidate)
Vue.use(VueResource);
Vue.use(VueMoment);
Vue.config.productionTip = false;
Vue.prototype.moment = moment

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});