import { userService } from '../_services';

const state = {
    profile: {},
    starred:{}
};

const actions = {
    getProfile({ commit }) {
        commit('getProfileRequest');
        state.starred = {};
        userService.getProfile()
            .then(
                users => commit('getProfileSuccess', users),
                error => commit('getProfileFailure', error)
            );
    },

    getStarredRepo({ commit }) {
        commit('getStarredRepoRequest');

        userService.getStarredRepos()
            .then(
                repos => commit('getStarredRepoSuccess',repos),
                error => commit('getStarredRepoFailure', error)
            );
    }
};

const mutations = {
    getProfileRequest(state) {
        state.profile = { loading: true };
    },
    getProfileSuccess(state, user) {
        state.profile = { profile: user.user, loading: false };
    },
    getProfileFailure(state, error) {
        state.profile = { error, loading: false };
    },
    getStarredRepoRequest(state) {
        state.starred = { loading: true };
    },
    getStarredRepoSuccess(state, repos) {
        console.log(repos.repos);
        state.starred = { items: repos.repos };
    },
    getStarredRepoFailure(state,error) {
        state.starred = { error };
    }
};

export const users = {
    namespaced: true,
    state,
    actions,
    mutations
};
